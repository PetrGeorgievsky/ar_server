﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ar_server.http_server;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Threading;
using data_transfer_library;
using System.Net;
using System.Net.Sockets;
using StreamJsonRpc;
using System.Net.NetworkInformation;
using Emgu.CV.CvEnum;
using System.Diagnostics;

namespace ar_server
{    
    class Program
    {
        
        static async Task Main(string[] args)
        {
            Console.OutputEncoding = new UTF8Encoding();
            ARService service = new ARService();
            var hostName = Dns.GetHostName();
            var hostEntry = await Dns.GetHostEntryAsync(hostName);
            Console.WriteLine($"HostName:{hostEntry.HostName}");

            foreach (var alias in hostEntry.Aliases)
                Console.WriteLine($"HostAlias:{alias}");

            var addresses = hostEntry.AddressList;
            Console.WriteLine($"HostAddressCount:{addresses.Length}");

            foreach (var addr in addresses)
                Console.WriteLine($"HostAddress:{addr.ToString()}");

            Console.WriteLine("HostAddressEnd");
            IPAddress address=null;
            foreach (var addr in addresses)
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    address = addr;
                    break;
                }

            TcpListener listener = new TcpListener(address, 49158);
            listener.Start();
            while (true)
            {
                var client = listener.AcceptTcpClientAsync();
                try
                {
                    var client_res = await client;
                    var stream = client_res.GetStream();
                    var rpc = JsonRpc.Attach(stream, service);
                    rpc.TraceSource.Listeners.Add(new ConsoleTraceListener());
                    await rpc.Completion;
                    client_res.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

    }
}
