﻿using data_stream_pool_lib;
using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ar_server
{
    public interface IARService
    {
        Task<string> GetAuthTokenAsync(string machineId);
        Task AckAsync(string authToken);
        Task<byte[]> GetSceneDataAsync(string authToken, string sceneId);
        Task<Tuple<string, int, string>> InitVideoStreamAsync(string authToken, string sceneId, string dataRecieveIP, int dataRecievePort);
        Task CloseVideoStreamAsync(string authToken, string videoStreamToken);

    }
    public class ConnectionInfo
    {
        public DateTime LastAck;
        public string MachineId;
        public string StreamId;
        public string PoolId;
    }
    public class DataStreamPoolInfo
    {
        public IPEndPoint EndPoint;
    }
    public class ARService : IARService
    {
        Dictionary<Guid, ConnectionInfo> authValidators=new Dictionary<Guid, ConnectionInfo>();
        KeyValuePair<string,IDataStreamPool>? localStreamPool=null;
        List<TcpClient> dataStreamPoolClients=new List<TcpClient>();
        Dictionary<string, IDataStreamPool> dataStreamPools = new Dictionary<string, IDataStreamPool>();
        Timer tidyUpEventTimer;
        public ARService()
        {
            var _localStreamPool = new DataStreamPool(100, new DataTransferPluginManager("transfer_plugins"));

            localStreamPool = new KeyValuePair<string, IDataStreamPool>(_localStreamPool.GetStatus().Id, _localStreamPool);
            tidyUpEventTimer = new Timer(1000*60*15);
            tidyUpEventTimer.Elapsed += TidyUpEventTimer_Elapsed;
            tidyUpEventTimer.AutoReset = true;
            tidyUpEventTimer.Start();
        }

        private void TidyUpEventTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Очистка пула соединений начата");
            TidyUp();
            Console.WriteLine("Очистка пула соединений завершена");
        }

        public ARService( List<string> server_ip_list ):this()
        {
            foreach (var server_ip in server_ip_list)
            {
                TcpClient tcpClient = new TcpClient(server_ip, 49159);
                tcpClient.GetStream();
                var stream = tcpClient.GetStream();
                var rpc = JsonRpc.Attach<IDataStreamPool>(stream);
                var status = rpc.GetStatus();
                dataStreamPools.Add(status.Id, rpc);
                dataStreamPoolClients.Add(tcpClient);
            }
        }

        KeyValuePair<string, IDataStreamPool>? GetPreferredPool()
        {
            var poolList = from p in dataStreamPools
                           orderby p.Value.GetStatus().FreeStreamCount
                           where p.Value.GetStatus().FreeStreamCount > 0
                           select p;
            try
            {
                return poolList.ElementAt(0);
            }
            catch
            {
                return localStreamPool;
            }
        }

        IDataStreamPool GetPoolById(string id)
        {
            IDataStreamPool pool = null;
            if (!dataStreamPools.TryGetValue(id, out pool))
            {
                pool = localStreamPool?.Value;
            }
            return pool;
        }
        void TidyUp()
        {
            foreach( var auth_validator in authValidators.ToArray())
            {
                var val = auth_validator.Value;
                if ( DateTime.Now - val.LastAck > TimeSpan.FromMinutes(15) )
                {
                    if(val.PoolId !=null)
                    GetPoolById(val.PoolId)?.CloseConnection(val.StreamId);
                    authValidators.Remove(auth_validator.Key);
                }
            }
        }

        public async Task AckAsync(string authToken)
        {
            
            ConnectionInfo info;
            if (!authValidators.TryGetValue(Guid.Parse(authToken), out info))
                throw new UnauthorizedAccessException();
            info.LastAck = DateTime.Now;
        }

        public async Task<Tuple<string, int, string>> InitVideoStreamAsync(string authToken, string sceneId, string dataRecieveIP, int dataRecievePort)
        {
            ConnectionInfo info;
            if (!authValidators.TryGetValue(Guid.Parse(authToken), out info))
                throw new UnauthorizedAccessException();
            var pool = GetPreferredPool();
            info.PoolId = pool?.Key;
            var res = pool?.Value.InitConnection(dataRecieveIP, dataRecievePort, "ArucoDetectorTransform", sceneId);
            info.StreamId = res.Item3;
            return new Tuple<string, int, string>( res.Item1, res.Item2, res.Item3 );
            //throw new NotImplementedException();
        }

        public async Task CloseVideoStreamAsync(string authToken, string videoStreamToken)
        {
            ConnectionInfo info;
            if (!authValidators.TryGetValue(Guid.Parse(authToken), out info))
                throw new UnauthorizedAccessException();
            IDataStreamPool pool= GetPoolById(info.PoolId);
            pool.CloseConnection(info.MachineId);
        }

        public async Task<string> GetAuthTokenAsync(string machineId)
        {
            Guid authToken = Guid.NewGuid();
            authValidators[authToken] = new ConnectionInfo();
            authValidators[authToken].MachineId = machineId;
            authValidators[authToken].LastAck = DateTime.Now;
            return authToken.ToString();
        }

        public async Task<byte[]> GetSceneDataAsync(string authToken, string sceneId)
        {
            throw new NotImplementedException();
        }

    }
}
