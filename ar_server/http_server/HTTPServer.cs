﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ar_server.http_server
{
    public class HTTPServer
    {
        readonly TcpListener Listener;
        List<HTTPConnection> TcpClients;

        public HTTPServer(int port)
        {
            Listener = new TcpListener(IPAddress.Any, port);
            Listener.Start();
            while (true)
            {
                TcpClient client = null;
                try
                {
                    client = Listener.AcceptTcpClient();
                }
                catch( Exception ex )
                {
                    Console.Out.WriteLine(ex.Message);
                }
                if (client != null)
                {
                    HandleClient(client);
                    client.Close();
                }
            }
        }

        private void HandleClient(TcpClient client)
        {
            var stream = client.GetStream();
            var reader = new StreamReader(stream);
            var writer = new StreamWriter(stream, new UTF8Encoding(false));
            string msg = "";
            while(reader.Peek() != -1)
            {
                msg += reader.ReadLine() + "\n";
            }
            Console.WriteLine("Request :\n" + msg);
        }
    }
}
