﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using rtp_library;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;

namespace emgu_videostream_lib
{
    public class OutVideoStream
    {
        Thread rtpSenderThread = null;
        RTPSender rtpSender = null;
        VideoCapture capture = null;
        protected uint timestamp = 0;
        protected ushort id = 0;
        bool working = true;
        public OutVideoStream(int port)
        {
            rtpSender = new RTPSender(port);
            capture = new VideoCapture(0);
            Console.Out.WriteLine($"FrameSize:{capture.Width * 0.5},{capture.Height * 0.5}");
            rtpSenderThread = new Thread(Worker);
            rtpSenderThread.Start();
        }
        ~OutVideoStream()
        {
            working = false;
            rtpSenderThread.Join();
        }
        void Worker()
        {
            while (working)
            {
                Mat frame = capture.QuerySmallFrame();
                RTPHeader header;
                byte[] data;
                if (!EncodeFrame(frame, out header, out data))
                    continue;
                rtpSender.Send(header, data);
            }
        }
        public virtual bool EncodeFrame(Mat frame, out RTPHeader header, out byte[] res_data)
        {
            res_data = null;
            header = null;
            return false;
        }
    }

    public class RAWJPEGOutVideoStream : OutVideoStream
    {
        public RAWJPEGOutVideoStream(int port) : base(port)
        {
        }

        public override bool EncodeFrame(Mat frame, out RTPHeader header, out byte[] res_data)
        {
            Image<Bgr, byte> image = frame.ToImage<Bgr, byte>();

            header = RTPHeader.CreateHeader(timestamp, id, RTPPayloadType.JPEG);
            res_data = image.ToJpegData();

            if (header != null && res_data != null)
                return true;
            else
                return false;
        }
    }

}
