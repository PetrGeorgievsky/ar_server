﻿using rtp_library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace emgu_videostream_lib
{
    public class InVideoStream
    {
        RTPReciever Reciever;
        Thread rtpRecieverThread = null;
        bool working = true;

        public InVideoStream(IPEndPoint sender_ip)
        {
            Reciever = new RTPReciever(sender_ip);
            rtpRecieverThread = new Thread(Worker);
            rtpRecieverThread.Start();
        }
        ~InVideoStream()
        {
            working = false;
            rtpRecieverThread.Join();
        }
        void Worker()
        {
            while (working)
            {
                byte[] res = Reciever.Recieve();
                OnRecieve(res);
            }
        }
        public virtual void OnRecieve(byte[] data)
        {
        }
    }
    /// <summary>
    /// Сбрасывает входящий поток в файловую систему
    /// </summary>
    public class TestInVideoStream: InVideoStream
    {
        public TestInVideoStream(IPEndPoint sender_ip) : base(sender_ip)
        {
        }

        public override void OnRecieve(byte[] data)
        {
            if (data == null || data.Length == 0)
                return;
            try
            {
                using (var fileStream = new FileStream("img.jpg", FileMode.Create))
                {
                    fileStream.Write(data, 0, data.Length);
                }
            }
            catch( Exception ex )
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
