﻿using ARucoMarkerDetector;
using data_transfer_library;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace test_console_app
{
    class TestDataReciever : UDPReciever
    {
        BinaryFormatter formatter = new BinaryFormatter();
        int tick = 0;
        public TestDataReciever(IPEndPoint recieverEndPoint) : base(recieverEndPoint)
        {

        }
        public override byte[] Recieve()
        {
            tick++;
            var buf = base.Recieve();
            MemoryStream stream = new MemoryStream(buf);
            ArucoDetectorResult detectorResult = formatter.Deserialize(stream) as ArucoDetectorResult;

            Console.WriteLine($"Detector result:{detectorResult.DetectedMarkersCount}");
            Console.WriteLine($"Recieve:s{DateTime.UtcNow.Second} ms{DateTime.UtcNow.Millisecond}");
            return null;
        }
    }
    class TestDataGenerator : IDataReciever
    {

        VideoCapture capture = null;
        Mat framebuffer = null;
        int tick = 0;
        public TestDataGenerator(string file_path)
        {
            capture = new VideoCapture(0);
            framebuffer = new Mat(128, 128, DepthType.Default, 3);
            Console.WriteLine(capture.GetCaptureProperty(CapProp.Fps));
            capture.SetCaptureProperty(CapProp.FrameWidth, 256);
            capture.SetCaptureProperty(CapProp.FrameHeight, 256);
        }

        public void Close()
        {
        }

        public byte[] Recieve()
        {
            Mat frame = new Mat();
            capture.Read(frame);
            CvInvoke.Resize(frame, framebuffer, framebuffer.Size);
            tick++;

            Console.WriteLine($"Send:s{DateTime.UtcNow.Second} ms{DateTime.UtcNow.Millisecond}");
            return framebuffer.GetRawData();//Encoding.ASCII.GetBytes($"{DateTime.UtcNow.Ticks}");
        }
    }

    public interface IARService
    {
        Task<string> GetAuthTokenAsync(string machineId);
        Task AckAsync(string authToken);
        Task<ulong> GetSceneDataAsync(string authToken, string sceneId);
        Task<Tuple<string, int, string>> InitVideoStreamAsync(string authToken, string sceneId, string dataRecieveIP, int dataRecievePort);
        Task CloseVideoStreamAsync(string authToken, string videoStreamToken);

    }
    class Program
    {
        public static ushort GetAvailablePort(ushort startingPort)
        {
            IPEndPoint[] endPoints;
            List<int> portArray = new List<int>();

            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();

            //getting active connections
            TcpConnectionInformation[] connections = properties.GetActiveTcpConnections();
            portArray.AddRange(from n in connections
                               where n.LocalEndPoint.Port >= startingPort
                               select n.LocalEndPoint.Port);

            //getting active tcp listners - WCF service listening in tcp
            endPoints = properties.GetActiveTcpListeners();
            portArray.AddRange(from n in endPoints
                               where n.Port >= startingPort
                               select n.Port);

            //getting active udp listeners
            endPoints = properties.GetActiveUdpListeners();
            portArray.AddRange(from n in endPoints
                               where n.Port >= startingPort
                               select n.Port);

            portArray.Sort();

            for (ushort i = startingPort; i < ushort.MaxValue; i++)
                if (!portArray.Contains(i))
                    return i;

            return 0;
        }
        class EmptyTransform : IDataTransform
        {
            public void Init(object initData)
            {
            }

            public byte[] Transform(byte[] data)
            {
                return data;
            }
        }
        static async Task Main(string[] args)
        {
            Console.OutputEncoding = new UTF8Encoding();
            TcpClient tcpClient = new TcpClient("192.168.0.100", 49158);
            var stream = tcpClient.GetStream();
            var rpc = JsonRpc.Attach<IARService>(stream);
            

            var dataRecieveEP = new IPEndPoint(IPAddress.Loopback, GetAvailablePort(1000));
            Thread udpRecieverThread = new Thread(() =>
            {
                var reciever = new TestDataReciever(dataRecieveEP);
                while (true)
                {
                    reciever.Recieve();
                }
            });
            udpRecieverThread.Start();

            var dataSendEP = await rpc.InitVideoStreamAsync("testToken","testScene", dataRecieveEP.Address.ToString(), dataRecieveEP.Port);
            tcpClient.Close();
            DataTransferProcess transferProcess = new DataTransferProcess(new UDPSender(0, new IPEndPoint(IPAddress.Parse(dataSendEP.Item1), dataSendEP.Item2)),
                new TestDataGenerator("test"),new EmptyTransform(), DataTransferType.OnDemand);
        }
    }
}
