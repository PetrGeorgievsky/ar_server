﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using data_transfer_library;
using data_stream_pool_lib;
using StreamJsonRpc;
using System.Threading.Tasks;
using System.Diagnostics;

namespace data_stream_pool
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.OutputEncoding = new UTF8Encoding();
            DataTransferPluginManager transferPluginManager = new DataTransferPluginManager("transfer_plugins");
            DataStreamPool dataStreamPool = new DataStreamPool(100, transferPluginManager);

            TcpListener listener = new TcpListener(IPAddress.Loopback, 49159);
            listener.Start();

            while (true)
            {
                var client = listener.AcceptTcpClientAsync();
                try
                {
                    var client_res = await client;
                    var stream = client_res.GetStream();
                    var rpc = JsonRpc.Attach(stream, dataStreamPool);
                    await rpc.Completion;
                    client_res.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
