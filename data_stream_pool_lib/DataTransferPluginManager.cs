﻿using data_transfer_library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace data_stream_pool_lib
{
    public class DataTransferPluginManager
    {
        Dictionary<string, Type> dataTransformTypes= new Dictionary<string, Type>();
        public DataTransferPluginManager(string plugins_assembly_path)
        {
            // Load assembly from path
            var plugins = Directory.GetFiles(plugins_assembly_path, "*.dll");
            foreach (var plugin in plugins)
            {
                var assembly = Assembly.LoadFrom(plugin);
                var types = assembly.GetExportedTypes();
                var implement_type = typeof(IDataTransform);
                foreach (var type in types)
                {
                    if (implement_type.IsAssignableFrom(type))
                        dataTransformTypes.Add(type.Name, type);
                }
            }
            foreach(var keyval in dataTransformTypes)
            {
                Console.WriteLine($"Тип преобразования данных {keyval.Key} успешно загружен в менеджер плагинов.");
            }
        }

        public IDataTransform GetDataTransform(string name, object initData)
        {
            if (!dataTransformTypes.TryGetValue(name, out Type transformType))
                return null;
            IDataTransform dataTransform = Activator.CreateInstance(transformType) as IDataTransform;
            dataTransform.Init(initData);
            return dataTransform;
        }
    }
}
