﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using data_transfer_library;
using shared_utils;

namespace data_stream_pool_lib
{
    public struct DataStreamPoolInfo
    {
        public uint FreeStreamCount;
        public uint Capacity;
        public string Id;
    }

    public interface IDataStreamPool
    {
        Tuple<string, int, string> InitConnection(string dataRecieveIP, int dataRecievePort, string dataTransform, object initData);
        void CloseConnection(string streamId);

        DataStreamPoolInfo GetStatus();
    }

    public class DataStreamPool: IDataStreamPool
    {
        private IPAddress Address;
        DataStreamPoolInfo PoolInfo;
        DataTransferPluginManager pluginManager;
        List<DataTransferProcess> transferProcessPool = new List<DataTransferProcess>();

        public DataStreamPool(uint capacity, DataTransferPluginManager transferPluginManager)
        {
            PoolInfo.Id = Guid.NewGuid().ToString();
            PoolInfo.Capacity = capacity;
            PoolInfo.FreeStreamCount = capacity;
            pluginManager = transferPluginManager;
            var hostName = Dns.GetHostName();
            var hostEntry = Dns.GetHostEntry(hostName);
            Console.WriteLine($"HostName:{hostEntry.HostName}");

            foreach(var alias in hostEntry.Aliases)
                Console.WriteLine($"HostAlias:{alias}");

            var addresses = hostEntry.AddressList;
            Console.WriteLine($"HostAddressCount:{addresses.Length}");

            foreach(var addr in addresses)
                Console.WriteLine($"HostAddress:{addr.ToString()}");
            
            Console.WriteLine("HostAddressEnd");
            foreach (var addr in addresses)
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    Address = addr;
                    break;
                }

            Console.WriteLine($"Selected Host Address:{Address.ToString()}");
        }

        /// <summary>
        /// Инициализирует двухстороннее соединение с пулом обработчиков
        /// </summary>
        /// <param name="dataSendEP">Конечная точка с которой будут приходить данные на обработчик</param>
        /// <param name="dataRecieveEP">Конечная точка, на которую будут приходить данные с обработчика</param>
        public Tuple<string, int, string> InitConnection(string dataRecieveIP, int dataRecievePort, string dataTransform, object initData)
        {
            transferProcessPool.RemoveAll(p => !p.IsRunning);
            PoolInfo.FreeStreamCount = PoolInfo.Capacity - (uint)transferProcessPool.Count;

            if (PoolInfo.FreeStreamCount <= 0)
                throw new Exception("Pool is full");

            var recieveEP = new Tuple<string, int>(Address.ToString(), Utility.GetAvailablePort(1000));

            IDataSender sender = new UDPSender( 0, new IPEndPoint(IPAddress.Parse(dataRecieveIP), dataRecievePort) );
            IDataReciever reciever = new RTPReciever( new IPEndPoint(IPAddress.Parse(recieveEP.Item1), recieveEP.Item2) );
            IDataTransform transform = pluginManager.GetDataTransform(dataTransform, initData);

            DataTransferProcess dataTransferProcess = new DataTransferProcess(sender, reciever, transform, DataTransferType.OnDemand);
            transferProcessPool.Add(dataTransferProcess);

            PoolInfo.FreeStreamCount--;

            return new Tuple<string, int, string>(recieveEP.Item1, recieveEP.Item2, dataTransferProcess.Id) ;
        }

        public DataStreamPoolInfo GetStatus()
        {
            return PoolInfo;
        }

        public void CloseConnection(string streamId)
        {
            var pool = transferProcessPool.Find(p => p.Id == streamId);
            pool.Close();
            transferProcessPool.RemoveAll(p => p.Id == streamId);
            
        }
    }
}
