﻿using System;
using System.Collections.Generic;
using System.Text;

namespace data_transfer_library
{
    public interface IDataReciever
    {
        byte[] Recieve();
        void Close();
    }
}
