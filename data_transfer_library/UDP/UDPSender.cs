﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Text;

namespace data_transfer_library
{
    public class UDPSender : IDataSender
    {
        private readonly UdpClient udpClient;
        private readonly IPEndPoint _recieverEndPoint;

        public UDPSender(int sendPort, IPEndPoint recieverEndPoint)
        {
            udpClient = new UdpClient(sendPort, recieverEndPoint.AddressFamily);
            _recieverEndPoint = recieverEndPoint;
        }
        ~UDPSender()
        {
            udpClient?.Close();
        }

        public virtual void Send(byte[] data)
        {
            if (data.Length > ushort.MaxValue)
                throw new NotImplementedException();
            udpClient.Send(data, data.Length, _recieverEndPoint);
        }
    }
}
