﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace data_transfer_library
{
    public class UDPReciever : IDataReciever
    {
        UdpClient client;
        IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any,0);

        public UDPReciever(IPEndPoint localEndPoint)
        {
            client = new UdpClient(localEndPoint);
        }

        ~UDPReciever()
        {
            client?.Close();
        }

        public void Close()
        {
            client?.Close();
        }

        public virtual byte[] Recieve()
        {
            return client.Receive(ref remoteEP);
        }
    }
}
