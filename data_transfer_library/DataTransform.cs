﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace data_transfer_library
{
    public interface IDataTransform
    {
        void Init(object initData);
        byte[] Transform(byte[] data);
    }
}
