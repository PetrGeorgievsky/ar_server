﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace data_transfer_library
{
    public enum DataTransferType
    {
        OnDemand,
        Parallel
    }
    public class DataTransferProcess
    {
        IDataSender sender;
        IDataReciever reciever;
        IDataTransform transform;
        DataTransferType transferType;

        Thread dataTransferThread = null;
        Thread dataSendThread = null;
        byte[] storage;
        object mutex = new object();
        bool sendThreadRunning = false;
        bool transferThreadRunning = false;

        public bool IsRunning { get; private set; } = false;
        public string Id { get; private set; } = Guid.NewGuid().ToString();

        public DataTransferProcess(IDataSender dataSender, IDataReciever dataReciever, IDataTransform dataTransform, DataTransferType dataTransferType)
        {
            sender = dataSender;
            reciever = dataReciever;
            transform = dataTransform;
            transferType = dataTransferType;
            IsRunning = true;
            switch (transferType)
            {
                case DataTransferType.OnDemand:
                    transferThreadRunning = true;
                    dataTransferThread = new Thread(OnDemandWorker);
                    dataTransferThread.Start();
                    break;
                case DataTransferType.Parallel:

                    transferThreadRunning = true;
                    dataTransferThread = new Thread(ConstantRecieveWorker);
                    dataTransferThread.Start();

                    sendThreadRunning = true;
                    dataSendThread = new Thread(ConstantSendWorker);
                    dataSendThread.Start();

                    break;
            }
        }

        ~DataTransferProcess()
        {
            dataTransferThread?.Join();
            dataSendThread?.Join();
        }

        public void Close()
        {
            transferThreadRunning = false;
            sendThreadRunning = false;
            reciever?.Close();
        }

        void OnDemandWorker()
        {
            while (transferThreadRunning)
            {
                try
                {
                    var data = reciever?.Recieve();
                    if (data?.Length > 0)
                    {
                        var transformedData = transform.Transform(data);
                        if(transformedData.Length > 0)
                            sender.Send(transformedData);
                    }
                }
                catch( Exception ex )
                {
                    transferThreadRunning = false;
                    sendThreadRunning = false;
                    Console.WriteLine(ex.Message);
                    break;
                }
            }
            IsRunning = false;
        }

        void ConstantRecieveWorker()
        {
            while (transferThreadRunning)
            {
                lock (mutex)
                {
                    storage = reciever.Recieve();
                }
            }
            IsRunning = false;
        }

        void ConstantSendWorker()
        {
            while (sendThreadRunning)
            {
                if (storage?.Length > 0)
                {
                    var data = transform.Transform(storage);
                    if(data.Length > 0)
                        sender.Send(data);
                }
            }
            IsRunning = false;
        }
    }
}
