﻿using System;
using System.Collections.Generic;
using System.Text;

namespace data_transfer_library
{
    public interface IDataSender
    {
        void Send(byte[] data);
    }
}
