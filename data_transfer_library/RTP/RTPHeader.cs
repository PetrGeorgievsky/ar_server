﻿using System;

namespace data_transfer_library
{
    public enum RTPPayloadType
    {
        JPEG,
        RAW
    }
    public class MJPEGPayload
    {
        byte TypeSpecific;
        int FragmentOffset = 0;
        byte Type = 0;
        byte Q = 0;
        byte Width, Height;
        // TODO: реализовать полноценный пэйлоад MJPEG 
    }
    public class RTPHeader
    {
        byte Version = 2;
        bool HasPadding;
        bool HasExtensions;
        bool Marker;
        byte PayloadType;
        ushort SequenceNumber;
        uint TimeStamp;

        public static RTPHeader CreateHeader(uint timestamp, ushort id, RTPPayloadType payloadType)
        {
            RTPHeader header = new RTPHeader();
            header.TimeStamp = timestamp;
            header.SequenceNumber = id;
            switch (payloadType)
            {
                case RTPPayloadType.JPEG:
                    header.PayloadType = 26;
                    break;
                case RTPPayloadType.RAW:
                    header.PayloadType = 112;
                    break;
                default:
                    throw new NotImplementedException("Неизвестный пэйлоад!");
            }
            return header;
        }

        public static byte[] ConvertToDatagramm(RTPHeader header)
        {
            byte[] datagramm = new byte[8];
            datagramm[0] = header.Version;

            // в первом байте пишем только версию, остальное возможно добавим позже
            //if (header.HasPadding)
            //    datagramm[0] |= 0b0000_0100;
            //
            datagramm[1] = header.PayloadType;

            if(header.Marker)
                datagramm[1] |= 0b1000_0000;

            datagramm[2] = (byte)(header.SequenceNumber >> 8);
            datagramm[3] = (byte)(header.SequenceNumber & 0xFF);

            datagramm[4] = (byte)(header.TimeStamp >> 24);
            datagramm[5] = (byte)((header.TimeStamp >> 16) & 0xFF);
            datagramm[6] = (byte)((header.TimeStamp >> 8) & 0xFF);
            datagramm[7] = (byte)(header.TimeStamp & 0xFF);

            return datagramm;
        }
    }
}
