﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace data_transfer_library
{
    public class RTPSender : UDPSender
    {
        RTPHeader header;
        private short sequenceNum;
        public RTPSender(int sendPort, IPEndPoint recieverEndPoint, RTPPayloadType payloadType): base(sendPort, recieverEndPoint)
        {
            header = RTPHeader.CreateHeader(0, 0, payloadType);
        }

        public override void Send(byte[] data) 
        {
            byte[] headerBytes = RTPHeader.ConvertToDatagramm(header);
            byte[] resultDatagramm = new byte[data.Length + headerBytes.Length];

            headerBytes.CopyTo(resultDatagramm, 0);
            data.CopyTo(resultDatagramm, headerBytes.Length);

            base.Send(resultDatagramm);
        }
    }
}
