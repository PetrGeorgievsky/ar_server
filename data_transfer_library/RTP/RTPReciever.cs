﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace data_transfer_library
{
    public class RTPReciever: UDPReciever
    {
        public RTPReciever(IPEndPoint localEndPoint):base(localEndPoint)
        {
        }

        // TODO: оптимизировать если можно, например убрать аллокацию и использовать преаллоцированный буффер
        public override byte[] Recieve()
        {
            byte[] packet, datagramm;

            datagramm = base.Recieve();

            packet = new byte[datagramm.Length - 8];

            Array.Copy(datagramm, 8, packet, 0, datagramm.Length - 8);

            return packet;
        }
    }
}
