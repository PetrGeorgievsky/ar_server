﻿using data_transfer_library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.Aruco;
using Emgu.CV;
using System.Runtime.InteropServices;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.CV.Structure;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Drawing;
using Newtonsoft.Json;

namespace ARucoMarkerDetector
{
    [Serializable]
    public class ArucoDetectorResult
    {
        public int DetectedMarkersCount { get; set; }

        public List<PointF[]> Corners { get; set; }
        public List<int> Ids { get; set; }

        public ArucoDetectorResult(VectorOfVectorOfPointF corners, VectorOfInt ids)
        {
            DetectedMarkersCount = Math.Min(ids.Size, 10);
            Corners = new List<PointF[]>(DetectedMarkersCount);
            Ids = new List<int>(DetectedMarkersCount);
            for(var i =0;i< DetectedMarkersCount; i++)
            {
                Corners.Add(corners[i].ToArray());
                Ids.Add(ids[i]);
            }
        }
    }
    public class ArucoDetectorTransform : IDataTransform
    {
        Dictionary dictionary = null;
        VectorOfVectorOfPointF corners = new VectorOfVectorOfPointF();
        VectorOfInt ids = new VectorOfInt();
        BinaryFormatter formatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream();

        DetectorParameters detectorParameters= DetectorParameters.GetDefault();
        public void Init(object initData)
        {
            dictionary = new Dictionary(Dictionary.PredefinedDictionaryName.Dict4X4_100);
        }

        public byte[] Transform(byte[] data)
        {
            memoryStream.SetLength(0);
            Mat m = new Mat();
            CvInvoke.Imdecode(data, ImreadModes.AnyColor, m);
            //GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            //Mat m = new Mat(128, 128, DepthType.Cv8U, 3, handle.AddrOfPinnedObject(), 128 * 3);
            //handle.Free();
            ArucoInvoke.DetectMarkers(m, dictionary, corners, ids, detectorParameters);

            var result = new ArucoDetectorResult(corners, ids);
            string responseString = JsonConvert.SerializeObject(result);
            return Encoding.UTF8.GetBytes(responseString);
        }
    }
}
