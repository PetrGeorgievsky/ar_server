﻿using data_transfer_library;
using Emgu.CV;
using Emgu.CV.CvEnum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BlurTransform
{
    public class BlurTransform : IDataTransform
    {
        public void Init(object initData)
        {
            ;
        }

        public byte[] Transform(byte[] data)
        {
            GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            Mat m = new Mat(128, 128, DepthType.Cv8U, 3, handle.AddrOfPinnedObject(), 128 * 3);
            handle.Free();
            CvInvoke.GaussianBlur(m, m, new Size(0, 0), 16);
            return m.GetRawData();
        }
    }
}
